function createNewUser() {
    const newUser = {
      firstName: prompt("Введіть ваше ім'я"),
      lastName: prompt("Введіть ваше прізвище"),
      getLogin() {
        const firstLetter = this.firstName.charAt(0).toLowerCase();
        return `${firstLetter}${this.lastName.toLowerCase()}`;
      }
    };
    
    return newUser;
  }

// createNewUser();
console.log(createNewUser().getLogin());

// const anotherUser = createNewUser();
// console.log(anotherUser.getLogin());
